﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for Agilent,ESA and PSA series.

1.0: Initial Release, Spectrum Analyzer personality supported.
2.0: Phase Noise and Noise Figure personalities are now supported.
2.1: Graphs are added to Log Plot and Monitor Spectrum.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(_!!!*Q(C=\&gt;4"=2J"%!81FMM(8]H!21K&gt;!CG11K@!V5&gt;3["2)A21["6,A[+P]7)V&gt;0EE8OWR6;6;.Y-`MT'.WW9CN@9HY(-^PN?_PNP@@([_WD`[0`P@&gt;`U9TZNO&lt;A`ZSCXD;J'J5KV+Z@&lt;PHE9^]Z#-@_=B&lt;XP+7N\TF,3^ZS5N?]J+80/5J4XH+5Z\S\:#(0/1BNSND]&lt;(1G(2--!;DR(A:HO%:HO(R-9:H?):H?)&lt;(E"C?Y2G?Y2E?J]8Q$-`Q$-`QG'K4T(&lt;)?9&lt;(^.%]T&gt;-]T&gt;-]FITG;9#W7*OY49)2L&lt;/^;:\G;2ZPIXG;JXG;JXFU2`-U4`-U4`-Y:&gt;O6XD301]ZDGCC?YCG?YCE?5U@R&amp;%`R&amp;%`R7#[+JXA+ICR9*E?)=F):5$Y54`(Y&amp;]640-640-7D;\N#N?X-1`-YZ$T*ETT*ETT*9YJ)HO2*HO2*(N.']C20]C20]FAKEC&gt;ZEC&gt;"UK+7DT2:/D%.3E(S_.POFNSO5G_3X)\VUXSZ570&gt;A,&amp;OL&amp;AX4+Q&lt;)&gt;9&amp;DH8B9FW17"M&gt;;Q.D&lt;5SM,RTLC]1#RFIYVI3R"M&lt;&gt;[UV&gt;V57&gt;V5E&gt;V5(NV?ZR[B]??,`@YX;\R@6[D=PF%O@T/5[H5RS0RTA=$L(@\W/XW`V[7HVVP,3HH]_F&lt;^\`CXJZ,PU0T]&lt;Y&amp;-_`0?&lt;NU1_[`-%`!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">2.1.1.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Item Name="Action-Status" Type="Folder">
			<Item Name="Query Phase Noise Marker.vi" Type="VI" URL="../Public/Action-Status/Query Phase Noise Marker.vi"/>
			<Item Name="Get Limit State.vi" Type="VI" URL="../Public/Action-Status/Get Limit State.vi"/>
			<Item Name="Calibrate Noise Figure.vi" Type="VI" URL="../Public/Action-Status/Calibrate Noise Figure.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Advanced" Type="Folder">
				<Item Name="Configure Amplitude Characteristics.vi" Type="VI" URL="../Public/Configure/Advanced/Configure Amplitude Characteristics.vi"/>
				<Item Name="Configure Correction.vi" Type="VI" URL="../Public/Configure/Advanced/Configure Correction.vi"/>
				<Item Name="Configure Demodulation.vi" Type="VI" URL="../Public/Configure/Advanced/Configure Demodulation.vi"/>
				<Item Name="Configure Gate Time.vi" Type="VI" URL="../Public/Configure/Advanced/Configure Gate Time.vi"/>
				<Item Name="Configure Limit Line.vi" Type="VI" URL="../Public/Configure/Advanced/Configure Limit Line.vi"/>
				<Item Name="Configure Radio Standard.vi" Type="VI" URL="../Public/Configure/Advanced/Configure Radio Standard.vi"/>
				<Item Name="Define Limit Line.vi" Type="VI" URL="../Public/Configure/Advanced/Define Limit Line.vi"/>
				<Item Name="Delete Correction.vi" Type="VI" URL="../Public/Configure/Advanced/Delete Correction.vi"/>
				<Item Name="Delete Limit Line.vi" Type="VI" URL="../Public/Configure/Advanced/Delete Limit Line.vi"/>
			</Item>
			<Item Name="Marker" Type="Folder">
				<Item Name="Configure Marker Characteristics.vi" Type="VI" URL="../Public/Configure/Marker/Configure Marker Characteristics.vi"/>
				<Item Name="Configure Marker Frequency Counter.vi" Type="VI" URL="../Public/Configure/Marker/Configure Marker Frequency Counter.vi"/>
				<Item Name="Configure Marker Search.vi" Type="VI" URL="../Public/Configure/Marker/Configure Marker Search.vi"/>
				<Item Name="Configure Marker Type.vi" Type="VI" URL="../Public/Configure/Marker/Configure Marker Type.vi"/>
				<Item Name="Marker Search.vi" Type="VI" URL="../Public/Configure/Marker/Marker Search.vi"/>
				<Item Name="Set Instrument From Marker.vi" Type="VI" URL="../Public/Configure/Marker/Set Instrument From Marker.vi"/>
			</Item>
			<Item Name="Measurement" Type="Folder">
				<Item Name="Configure Measurement (Adjacent Channel Power).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement (Adjacent Channel Power).vi"/>
				<Item Name="Configure Measurement (Burst Power).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement (Burst Power).vi"/>
				<Item Name="Configure Measurement (Channel Power).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement (Channel Power).vi"/>
				<Item Name="Configure Measurement (Harmonics Distortion).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement (Harmonics Distortion).vi"/>
				<Item Name="Configure Measurement (Intermod).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement (Intermod).vi"/>
				<Item Name="Configure Measurement (Multi-Carrier Power).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement (Multi-Carrier Power).vi"/>
				<Item Name="Configure Measurement (Occupied Bandwidth).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement (Occupied Bandwidth).vi"/>
				<Item Name="Configure Measurement (Power State CCDF).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement (Power State CCDF).vi"/>
				<Item Name="Configure Measurement (Spectrum Emissions Mask).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement (Spectrum Emissions Mask).vi"/>
				<Item Name="Configure Measurement (Spurious Emissions).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement (Spurious Emissions).vi"/>
				<Item Name="Configure Measurement Averaging.vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement Averaging.vi"/>
				<Item Name="Configure Measurement RRC Filter.vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement RRC Filter.vi"/>
				<Item Name="Configure Measurement.vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement.vi"/>
				<Item Name="Configure Multi-Carrier Power Carriers.vi" Type="VI" URL="../Public/Configure/Measurement/Configure Multi-Carrier Power Carriers.vi"/>
				<Item Name="Configure Offset And Limits List (Adjacent Channel Power).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Offset And Limits List (Adjacent Channel Power).vi"/>
				<Item Name="Configure Offset And Limits List (Multi-Carrier Power).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Offset And Limits List (Multi-Carrier Power).vi"/>
				<Item Name="Configure Offset And Limits List (Spectrum Emissions Mask - Frequency And Bandwidth).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Offset And Limits List (Spectrum Emissions Mask - Frequency And Bandwidth).vi"/>
				<Item Name="Configure Offset And Limits List (Spectrum Emissions Mask - Relative And Absolute Limits).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Offset And Limits List (Spectrum Emissions Mask - Relative And Absolute Limits).vi"/>
				<Item Name="Configure Offset And Limits List.vi" Type="VI" URL="../Public/Configure/Measurement/Configure Offset And Limits List.vi"/>
				<Item Name="Configure Range Table (Harmonic Distortion).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Range Table (Harmonic Distortion).vi"/>
				<Item Name="Configure Range Table (Spurious Emissions - Peak).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Range Table (Spurious Emissions - Peak).vi"/>
				<Item Name="Configure Range Table (Spurious Emissions - Frequency And Bandwidth).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Range Table (Spurious Emissions - Frequency And Bandwidth).vi"/>
				<Item Name="Configure Range Table (Spurious Emissions - Sweep And Limit).vi" Type="VI" URL="../Public/Configure/Measurement/Configure Range Table (Spurious Emissions - Sweep And Limit).vi"/>
				<Item Name="Configure Range Table.vi" Type="VI" URL="../Public/Configure/Measurement/Configure Range Table.vi"/>
				<Item Name="Disable Measurement.vi" Type="VI" URL="../Public/Configure/Measurement/Disable Measurement.vi"/>
				<Item Name="Optimize Reference Level.vi" Type="VI" URL="../Public/Configure/Measurement/Optimize Reference Level.vi"/>
			</Item>
			<Item Name="Trigger" Type="Folder">
				<Item Name="Configure Trigger Characteristics (RF Burst).vi" Type="VI" URL="../Public/Configure/Trigger/Configure Trigger Characteristics (RF Burst).vi"/>
				<Item Name="Configure Trigger Characteristics (TV).vi" Type="VI" URL="../Public/Configure/Trigger/Configure Trigger Characteristics (TV).vi"/>
				<Item Name="Configure Trigger Characteristics (Video).vi" Type="VI" URL="../Public/Configure/Trigger/Configure Trigger Characteristics (Video).vi"/>
				<Item Name="Configure Trigger Characteristics.vi" Type="VI" URL="../Public/Configure/Trigger/Configure Trigger Characteristics.vi"/>
				<Item Name="Configure Trigger.vi" Type="VI" URL="../Public/Configure/Trigger/Configure Trigger.vi"/>
			</Item>
			<Item Name="Phase Noise" Type="Folder">
				<Item Name="Configure Log Plot.vi" Type="VI" URL="../Public/Configure/Phase Noise/Configure Log Plot.vi"/>
				<Item Name="Configure RF Mixer Maximum Power.vi" Type="VI" URL="../Public/Configure/Phase Noise/Configure RF Mixer Maximum Power.vi"/>
				<Item Name="Configure Spot Frequency.vi" Type="VI" URL="../Public/Configure/Phase Noise/Configure Spot Frequency.vi"/>
				<Item Name="Configure Carrier Signal.vi" Type="VI" URL="../Public/Configure/Phase Noise/Configure Carrier Signal.vi"/>
				<Item Name="Configure Monitor Spectrum.vi" Type="VI" URL="../Public/Configure/Phase Noise/Configure Monitor Spectrum.vi"/>
				<Item Name="Configure Marker.vi" Type="VI" URL="../Public/Configure/Phase Noise/Configure Marker.vi"/>
				<Item Name="Configure DANL Measurement.vi" Type="VI" URL="../Public/Configure/Phase Noise/Configure DANL Measurement.vi"/>
			</Item>
			<Item Name="Noise Figure" Type="Folder">
				<Item Name="Configure System.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure System.vi"/>
				<Item Name="Configure RF Input.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure RF Input.vi"/>
				<Item Name="Configure Downconverter.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure Downconverter.vi"/>
				<Item Name="Configure Upconverter.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure Upconverter.vi"/>
				<Item Name="Configure Noise Source.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure Noise Source.vi"/>
				<Item Name="Configure NF Monitor Spectrum.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure NF Monitor Spectrum.vi"/>
				<Item Name="Configure NF Monitor Spectrum Frequency.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure NF Monitor Spectrum Frequency.vi"/>
				<Item Name="Configure NF Monitor Spectrum Bandwidth.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure NF Monitor Spectrum Bandwidth.vi"/>
				<Item Name="Configure Noise Figure.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure Noise Figure.vi"/>
				<Item Name="Configure Calibration ENR Table.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure Calibration ENR Table.vi"/>
				<Item Name="Configure Measurement ENR Table.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure Measurement ENR Table.vi"/>
				<Item Name="Configure NF Frequency.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure NF Frequency.vi"/>
				<Item Name="Configure Spot ENR.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure Spot ENR.vi"/>
				<Item Name="Configure DUT Loss Compensation.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure DUT Loss Compensation.vi"/>
				<Item Name="Configure NF Correction.vi" Type="VI" URL="../Public/Configure/Noise Figure/Configure NF Correction.vi"/>
			</Item>
			<Item Name="Configure Acquisition.vi" Type="VI" URL="../Public/Configure/Configure Acquisition.vi"/>
			<Item Name="Configure Averaging.vi" Type="VI" URL="../Public/Configure/Configure Averaging.vi"/>
			<Item Name="Configure Frequency (Center Span).vi" Type="VI" URL="../Public/Configure/Configure Frequency (Center Span).vi"/>
			<Item Name="Configure Frequency (Start Stop).vi" Type="VI" URL="../Public/Configure/Configure Frequency (Start Stop).vi"/>
			<Item Name="Configure Frequency Offset.vi" Type="VI" URL="../Public/Configure/Configure Frequency Offset.vi"/>
			<Item Name="Configure Frequency.vi" Type="VI" URL="../Public/Configure/Configure Frequency.vi"/>
			<Item Name="Configure Input Coupling.vi" Type="VI" URL="../Public/Configure/Configure Input Coupling.vi"/>
			<Item Name="Configure Level.vi" Type="VI" URL="../Public/Configure/Configure Level.vi"/>
			<Item Name="Configure Normalize.vi" Type="VI" URL="../Public/Configure/Configure Normalize.vi"/>
			<Item Name="Configure Sweep Coupling.vi" Type="VI" URL="../Public/Configure/Configure Sweep Coupling.vi"/>
			<Item Name="Configure Trace Operation (Add,Subtract).vi" Type="VI" URL="../Public/Configure/Configure Trace Operation (Add,Subtract).vi"/>
			<Item Name="Configure Trace Operation (Exchange,Copy).vi" Type="VI" URL="../Public/Configure/Configure Trace Operation (Exchange,Copy).vi"/>
			<Item Name="Configure Trace Operation (Subtract Display Line).vi" Type="VI" URL="../Public/Configure/Configure Trace Operation (Subtract Display Line).vi"/>
			<Item Name="Configure Trace Operation.vi" Type="VI" URL="../Public/Configure/Configure Trace Operation.vi"/>
			<Item Name="Configure Trace Type.vi" Type="VI" URL="../Public/Configure/Configure Trace Type.vi"/>
			<Item Name="Select Personality.vi" Type="VI" URL="../Public/Configure/Select Personality.vi"/>
			<Item Name="SA Continuous Mode.vi" Type="VI" URL="../Public/Configure/SA Continuous Mode.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Low Level" Type="Folder">
				<Item Name="Phase Noise" Type="Folder">
					<Item Name="Fetch Measurement (Carrier Power - Frequency).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Carrier Power - Frequency).vi"/>
					<Item Name="Fetch Measurement (RMS Phase Noise).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (RMS Phase Noise).vi"/>
					<Item Name="Fetch Measurement (Residual FM).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Residual FM).vi"/>
					<Item Name="Fetch Measurement (Spot Noise).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Spot Noise).vi"/>
					<Item Name="Fetch Measurement (Trace Data).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Trace Data).vi"/>
					<Item Name="Fetch Measurement (DANL Floor).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (DANL Floor).vi"/>
					<Item Name="Fetch Measurement (Monitor Spectrum).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Monitor Spectrum).vi"/>
					<Item Name="Fetch Measurement (Initial Carrier Frequency).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Initial Carrier Frequency).vi"/>
					<Item Name="Fetch Measurement (Carrier Frequency Deviation).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Carrier Frequency Deviation).vi"/>
					<Item Name="Fetch Measurement (Single Sideband Noise).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Single Sideband Noise).vi"/>
					<Item Name="Fetch Measurement (Carrier Phase Noise Data).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Carrier Phase Noise Data).vi"/>
					<Item Name="Fetch Measurement (Carrier Frequency Data).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Carrier Frequency Data).vi"/>
					<Item Name="Fetch Measurement (Carrier Power Data).vi" Type="VI" URL="../Public/Data/Low Level/Phase Noise/Fetch Measurement (Carrier Power Data).vi"/>
				</Item>
				<Item Name="Noise Figure" Type="Folder">
					<Item Name="Fetch Measurement (Gain).vi" Type="VI" URL="../Public/Data/Low Level/Noise Figure/Fetch Measurement (Gain).vi"/>
					<Item Name="Fetch Measurement (Noise Figure).vi" Type="VI" URL="../Public/Data/Low Level/Noise Figure/Fetch Measurement (Noise Figure).vi"/>
					<Item Name="Fetch Measurement (Noise Factor).vi" Type="VI" URL="../Public/Data/Low Level/Noise Figure/Fetch Measurement (Noise Factor).vi"/>
					<Item Name="Fetch Measurement (Cold Power Density).vi" Type="VI" URL="../Public/Data/Low Level/Noise Figure/Fetch Measurement (Cold Power Density).vi"/>
					<Item Name="Fetch Measurement (Hot Power Density).vi" Type="VI" URL="../Public/Data/Low Level/Noise Figure/Fetch Measurement (Hot Power Density).vi"/>
					<Item Name="Fetch Measurement (Effective Temperature).vi" Type="VI" URL="../Public/Data/Low Level/Noise Figure/Fetch Measurement (Effective Temperature).vi"/>
					<Item Name="Fetch Measurement (Tcold).vi" Type="VI" URL="../Public/Data/Low Level/Noise Figure/Fetch Measurement (Tcold).vi"/>
					<Item Name="Fetch Measurement (Y Factor).vi" Type="VI" URL="../Public/Data/Low Level/Noise Figure/Fetch Measurement (Y Factor).vi"/>
				</Item>
				<Item Name="Abort.vi" Type="VI" URL="../Public/Data/Low Level/Abort.vi"/>
				<Item Name="Fetch Measurement (Channel Power).vi" Type="VI" URL="../Public/Data/Low Level/Fetch Measurement (Channel Power).vi"/>
				<Item Name="Fetch Measurement (Harmonic Distortion).vi" Type="VI" URL="../Public/Data/Low Level/Fetch Measurement (Harmonic Distortion).vi"/>
				<Item Name="Fetch Measurement (Occupied Bandwidth).vi" Type="VI" URL="../Public/Data/Low Level/Fetch Measurement (Occupied Bandwidth).vi"/>
				<Item Name="Fetch Measurement (Others).vi" Type="VI" URL="../Public/Data/Low Level/Fetch Measurement (Others).vi"/>
				<Item Name="Fetch Measurement (Spectrum Emissions).vi" Type="VI" URL="../Public/Data/Low Level/Fetch Measurement (Spectrum Emissions).vi"/>
				<Item Name="Fetch Measurement.vi" Type="VI" URL="../Public/Data/Low Level/Fetch Measurement.vi"/>
				<Item Name="Fetch Phase Noise Measurement.vi" Type="VI" URL="../Public/Data/Low Level/Fetch Phase Noise Measurement.vi"/>
				<Item Name="Fetch Noise Figure Measurement.vi" Type="VI" URL="../Public/Data/Low Level/Fetch Noise Figure Measurement.vi"/>
				<Item Name="Fetch Trace.vi" Type="VI" URL="../Public/Data/Low Level/Fetch Trace.vi"/>
				<Item Name="Send Software Trigger.vi" Type="VI" URL="../Public/Data/Low Level/Send Software Trigger.vi"/>
				<Item Name="Initiate.vi" Type="VI" URL="../Public/Data/Low Level/Initiate.vi"/>
				<Item Name="Wait for Operation Complete.vi" Type="VI" URL="../Public/Data/Low Level/Wait for Operation Complete.vi"/>
			</Item>
			<Item Name="Phase Noise" Type="Folder">
				<Item Name="Read Measurement (Carrier Power - Frequency).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Carrier Power - Frequency).vi"/>
				<Item Name="Read Measurement (RMS Phase Noise).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (RMS Phase Noise).vi"/>
				<Item Name="Read Measurement (Residual FM).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Residual FM).vi"/>
				<Item Name="Read Measurement (Spot Noise).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Spot Noise).vi"/>
				<Item Name="Read Measurement (Trace Data).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Trace Data).vi"/>
				<Item Name="Read Measurement (DANL Floor).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (DANL Floor).vi"/>
				<Item Name="Read Measurement (Monitor Spectrum).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Monitor Spectrum).vi"/>
				<Item Name="Read Measurement (Initial Carrier Frequency).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Initial Carrier Frequency).vi"/>
				<Item Name="Read Measurement (Carrier Frequency Deviation).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Carrier Frequency Deviation).vi"/>
				<Item Name="Read Measurement (Single Sideband Noise).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Single Sideband Noise).vi"/>
				<Item Name="Read Measurement (Carrier Phase Noise Data).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Carrier Phase Noise Data).vi"/>
				<Item Name="Read Measurement (Carrier Frequency Data).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Carrier Frequency Data).vi"/>
				<Item Name="Read Measurement (Carrier Power Data).vi" Type="VI" URL="../Public/Data/Phase Noise/Read Measurement (Carrier Power Data).vi"/>
			</Item>
			<Item Name="Noise Figure" Type="Folder">
				<Item Name="Read Measurement (Gain).vi" Type="VI" URL="../Public/Data/Noise Figure/Read Measurement (Gain).vi"/>
				<Item Name="Read Measurement (Noise Figure).vi" Type="VI" URL="../Public/Data/Noise Figure/Read Measurement (Noise Figure).vi"/>
				<Item Name="Read Measurement (Noise Factor).vi" Type="VI" URL="../Public/Data/Noise Figure/Read Measurement (Noise Factor).vi"/>
				<Item Name="Read Measurement (Cold Power Density).vi" Type="VI" URL="../Public/Data/Noise Figure/Read Measurement (Cold Power Density).vi"/>
				<Item Name="Read Measurement (Hot Power Density).vi" Type="VI" URL="../Public/Data/Noise Figure/Read Measurement (Hot Power Density).vi"/>
				<Item Name="Read Measurement (Effective Temperature).vi" Type="VI" URL="../Public/Data/Noise Figure/Read Measurement (Effective Temperature).vi"/>
				<Item Name="Read Measurement (Tcold).vi" Type="VI" URL="../Public/Data/Noise Figure/Read Measurement (Tcold).vi"/>
				<Item Name="Read Measurement (Y Factor).vi" Type="VI" URL="../Public/Data/Noise Figure/Read Measurement (Y Factor).vi"/>
			</Item>
			<Item Name="Query Marker.vi" Type="VI" URL="../Public/Data/Query Marker.vi"/>
			<Item Name="Read Measurement (Channel Power).vi" Type="VI" URL="../Public/Data/Read Measurement (Channel Power).vi"/>
			<Item Name="Read Measurement (Harmonic Distortion).vi" Type="VI" URL="../Public/Data/Read Measurement (Harmonic Distortion).vi"/>
			<Item Name="Read Measurement (Occupied Bandwidth).vi" Type="VI" URL="../Public/Data/Read Measurement (Occupied Bandwidth).vi"/>
			<Item Name="Read Measurement (Others).vi" Type="VI" URL="../Public/Data/Read Measurement (Others).vi"/>
			<Item Name="Read Measurement (Spectrum Emissions).vi" Type="VI" URL="../Public/Data/Read Measurement (Spectrum Emissions).vi"/>
			<Item Name="Read Noise Figure Measurement.vi" Type="VI" URL="../Public/Data/Read Noise Figure Measurement.vi"/>
			<Item Name="Read Phase Noise Measurement.vi" Type="VI" URL="../Public/Data/Read Phase Noise Measurement.vi"/>
			<Item Name="Read Measurement.vi" Type="VI" URL="../Public/Data/Read Measurement.vi"/>
			<Item Name="Read Trace.vi" Type="VI" URL="../Public/Data/Read Trace.vi"/>
			<Item Name="Query Spur Marker.vi" Type="VI" URL="../Public/Data/Query Spur Marker.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="../Public/Utility/Self-Test.vi"/>
			<Item Name="Log Plot Save-Recall Setup.vi" Type="VI" URL="../Public/Utility/Log Plot Save-Recall Setup.vi"/>
			<Item Name="Phase Noise Display Setup.vi" Type="VI" URL="../Public/Utility/Phase Noise Display Setup.vi"/>
			<Item Name="Table Save-Recall Setup.vi" Type="VI" URL="../Public/Utility/Noise Figure/Table Save-Recall Setup.vi"/>
			<Item Name="Store Measurement Results.vi" Type="VI" URL="../Public/Utility/Noise Figure/Store Measurement Results.vi"/>
			<Item Name="Monitor Spectrum Display Setup.vi" Type="VI" URL="../Public/Utility/Noise Figure/Monitor Spectrum Display Setup.vi"/>
			<Item Name="Noise Figure Display Setup.vi" Type="VI" URL="../Public/Utility/Noise Figure/Noise Figure Display Setup.vi"/>
		</Item>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Classify Error.vi" Type="VI" URL="../Private/Classify Error.vi"/>
		<Item Name="Create Boolean Commands.vi" Type="VI" URL="../Private/Create Boolean Commands.vi"/>
		<Item Name="Create Double Commands.vi" Type="VI" URL="../Private/Create Double Commands.vi"/>
		<Item Name="Create Int Commands.vi" Type="VI" URL="../Private/Create Int Commands.vi"/>
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="../Private/Default Instrument Setup.vi"/>
	</Item>
</Library>
