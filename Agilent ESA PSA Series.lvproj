﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Name="Template - Generic.lvproj" Type="Project" LVVersion="11008008" URL="/&lt;instrlib&gt;/_Template - Generic/Template - Generic.lvproj">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This project is used by developers to edit API and example files for LabVIEW Plug and Play instrument drivers.</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Examples" Type="Folder">
			<Item Name="Agilent ESA PSA Noise Figure Calibration.vi" Type="VI" URL="../Examples/Agilent ESA PSA Noise Figure Calibration.vi"/>
			<Item Name="Agilent ESA PSA Noise Figure Measurement (ENR Table).vi" Type="VI" URL="../Examples/Agilent ESA PSA Noise Figure Measurement (ENR Table).vi"/>
			<Item Name="Agilent ESA PSA Noise Figure Measurement (Fixed Frequency).vi" Type="VI" URL="../Examples/Agilent ESA PSA Noise Figure Measurement (Fixed Frequency).vi"/>
			<Item Name="Agilent ESA PSA Noise Figure Monitor Spectrum Measurement.vi" Type="VI" URL="../Examples/Agilent ESA PSA Noise Figure Monitor Spectrum Measurement.vi"/>
			<Item Name="Agilent ESA PSA Phase Noise DANL Cancellation.vi" Type="VI" URL="../Examples/Agilent ESA PSA Phase Noise DANL Cancellation.vi"/>
			<Item Name="Agilent ESA PSA Phase Noise Log Plot Measurement.vi" Type="VI" URL="../Examples/Agilent ESA PSA Phase Noise Log Plot Measurement.vi"/>
			<Item Name="Agilent ESA PSA Phase Noise Marker Measurement.vi" Type="VI" URL="../Examples/Agilent ESA PSA Phase Noise Marker Measurement.vi"/>
			<Item Name="Agilent ESA PSA Phase Noise Monitor Spectrum Measurement.vi" Type="VI" URL="../Examples/Agilent ESA PSA Phase Noise Monitor Spectrum Measurement.vi"/>
			<Item Name="Agilent ESA PSA Phase Noise Multiple Spot Frequency Measurements.vi" Type="VI" URL="../Examples/Agilent ESA PSA Phase Noise Multiple Spot Frequency Measurements.vi"/>
			<Item Name="Agilent ESA PSA Phase Noise Spot Frequency Measurement.vi" Type="VI" URL="../Examples/Agilent ESA PSA Phase Noise Spot Frequency Measurement.vi"/>
			<Item Name="Agilent ESA PSA Phase Noise Spot Frequency Measurements With Signal Tracking.vi" Type="VI" URL="../Examples/Agilent ESA PSA Phase Noise Spot Frequency Measurements With Signal Tracking.vi"/>
			<Item Name="Agilent ESA PSA Series Acquire Measurement - Adjacent Channel Power.vi" Type="VI" URL="../Examples/Agilent ESA PSA Series Acquire Measurement - Adjacent Channel Power.vi"/>
			<Item Name="Agilent ESA PSA Series Acquire Trace - Triggered.vi" Type="VI" URL="../Examples/Agilent ESA PSA Series Acquire Trace - Triggered.vi"/>
			<Item Name="Agilent ESA PSA Series Acquire Trace.vi" Type="VI" URL="../Examples/Agilent ESA PSA Series Acquire Trace.vi"/>
			<Item Name="Agilent ESA PSA Series Configure and Query Marker.vi" Type="VI" URL="../Examples/Agilent ESA PSA Series Configure and Query Marker.vi"/>
			<Item Name="Agilent ESA PSA Series Limit Line.vi" Type="VI" URL="../Examples/Agilent ESA PSA Series Limit Line.vi"/>
			<Item Name="Agilent ESA PSA Series.bin3" Type="Document" URL="../Examples/Agilent ESA PSA Series.bin3"/>
		</Item>
		<Item Name="Agilent ESA PSA Series.lvlib" Type="Library" URL="../Agilent ESA PSA Series.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="../../../../../../../../../Program Files (x86)/National Instruments/LabVIEW 2011/resource/lvanlys.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
